# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Diego Mérida López <dmerida80@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-09 17:21+0100\n"
"PO-Revision-Date: 2022-10-22 21:02+0200\n"
"Last-Translator: Diego Mérida López <dmerida80@gmail.com>\n"
"Language-Team: Italian <dmerida80@gmail.com>\n"
"Language: It\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 41.0\n"

msgid "One Drive"
msgstr "One Drive"

msgid "Login..."
msgstr "Accedere..."

msgid "Show service status"
msgstr "Mostra lo stato del servizio"

msgid "Show service process"
msgstr "Mostra lo stato del processo"

msgid "Open One Drive web site"
msgstr "Apri il sito web di One Drive"

msgid "Open One Drive local folder"
msgstr "Apri la cartella locale di One Drive"

msgid "Logout..."
msgstr "Disconnettersi..."
